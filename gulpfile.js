const gulp = require('gulp')
const browserify = require('browserify')
const environments = require('gulp-environments')
const babelify = require('babelify')
const rename = require('gulp-rename')
const sourcemaps = require('gulp-sourcemaps')
const sass = require('gulp-sass')
// const eslint = require('gulp-eslint')
// var uglify = require('gulp-uglify')
// const eslintify = require('eslintify')
const gulpStylelint = require('gulp-stylelint')
const cleanCSS = require('gulp-clean-css')
const fontAwesome = require('node-font-awesome')
const autoprefixer = require('gulp-autoprefixer')
const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')
const pug = require('gulp-pug')
// const exec = require('gulp-exec')
// const gutil = require('gulp-util')

var development = environments.development
var production = environments.production

var SassPath = [
  './scss/**/*.+(scss|sass)'
]

var PugPath = [
  './templates/**/*.pug'
]

gulp.task('scss', function () {
  console.log('Compiling scss on ' + environments.current().$name + ' env.')
  gulp.src(SassPath)
    .pipe(development(sourcemaps.init()))
    .pipe(gulpStylelint({
      failAfterError: production(),
      reporters: [
        {formatter: 'string', console: true}
      ]
    }))
    .pipe(sass(
      {
        includePaths: [
          './node_modules/normalize-scss/sass',
          './node_modules/owl.carousel/src/scss',
          './node_modules/slick-carousel/slick',
          fontAwesome.scssPath
        ],
        style: development ? 'expanded' : 'compressed'
      }
    ).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(development(sourcemaps.write()))
    .pipe(production(cleanCSS()))
    .pipe(gulp.dest('./web/assets/css'))
})

// PUG BUILD HTML
gulp.task('views', () => {
  return gulp.src('./templates/*.pug')
    .pipe(pug({
      pretty: true,
      basedir: __dirname
    }))
    .pipe(gulp.dest('./web'))
})

// Basic usage
gulp.task('scripts', function () {
  // .pipe(eslint())
  // .pipe(eslint.format())
  var b = browserify('./js/index.js', {
    // debug: development
    // transform: [
    //   ['eslintify'],
    //   ['babelify', { presets: ['es2015'] }]
    // ]
  }).transform(babelify, {
    'presets': ['es2015'],
    sourceMaps: true
  })

  // browserify("./js/index.js").transform(babelify).bundle()
  // .on('error', function (err) { console.error(err); this.emit('end') })
  // Single entry point to browserify
  return b.bundle()
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe(development(sourcemaps.init({loadMaps: true})))
    .pipe(rename('bundle.js'))
    .pipe(development(sourcemaps.write()))
    .pipe(gulp.dest('./web/assets/js/'))

    // .on('error', gutil.log)
})

gulp.task('fonts', () => {
  gulp.src(fontAwesome.fonts)
    .pipe(gulp.dest('./web/assets/fonts/FontAwesome'))
  gulp.src('./node_modules/slick-carousel/slick/fonts/*')
    .pipe(gulp.dest('./web/assets/fonts/slick'))
  gulp.src('./fonts/Akrobat/*.+(ttf|woff|woff2)')
    .pipe(gulp.dest('./web/assets/fonts'))
  gulp.src('./fonts/Athelas/*.+(ttf|woff|woff2)')
    .pipe(gulp.dest('./web/assets/fonts'))
})

gulp.task('images', () => {
  gulp.src('node_modules/slick-carousel/slick/ajax-loader.gif')
    .pipe(gulp.dest('./web/assets/images/slick'))
  gulp.src('./media/images/**/*')
    .pipe(gulp.dest('./web/assets/images'))
})

gulp.task('video', () => {
  var videos = [
    './media/video/capsu.mp4',
    './media/video/capsu.webm'
  ]

  // Use only listed files
  gulp.src(videos)
    .pipe(gulp.dest('./web/assets/video'))

    // ToDo:
    // ffmpeg -i input.mov -vcodec libvpx -qmin 0 -qmax 50 -crf 10 -b:v 1M -acodec libvorbis output.webm
    // ffmpeg -i input.mov -vcodec h264 -acodec aac -strict -2 output.mp4
})

gulp.task('watch', ['scss', 'scripts', 'fonts', 'images', 'views', 'video'], function () {
  gulp.watch(SassPath, ['scss'])
  gulp.watch(PugPath, ['views'])
  gulp.watch('js/*.js', ['scripts'])
  gulp.watch('./media/**/*', ['images', 'video'])
})

gulp.task('eslint', () => {
  gulp.src('js/index.js')
})

// gulp.task('stylelint', () => {
//
// })

gulp.task('set-prod', production.task)
gulp.task('build', ['set-prod', 'scss', 'scripts', 'fonts', 'images', 'views', 'video'])

gulp.task('default', ['watch'])
