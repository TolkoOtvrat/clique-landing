const $ = require('jquery')
require('slick-carousel')

$(document).ready(function () {
  const navigation = $('.navigation', 'body')
  const navLink = $('a', navigation)
  const footerMenu = $('.menu > li > a', '.footer-top')
  const modalLabel = $('.openbtn', 'nav')
  const portContact = $('.contact-block > a', '#portfolio')

  navLink.click(function (e) {
    smoothScroll(e, this)
  })

  footerMenu.click(function (e) {
    smoothScroll(e, this)
  })

  portContact.click(function (e) {
    smoothScroll(e, this)
  })

  let smoothScroll = (e, link) => {
    e.preventDefault()
    $('html, body').animate({
      scrollTop: $($.attr(link, 'href')).offset().top
    }, 300)
  }

  const navMobile = $('.nav-mobile > i', 'body')

  navMobile.on('click', () => {
    modalLabel.toggleClass('opened')
    navigation.toggleClass('opened')
  })

  navLink.on('click', () => {
    modalLabel.toggleClass('opened')
    navigation.toggleClass('opened')
  })

  $('.wwd-carousel').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      }
    ]
  })

  /* Modal form */

  const modalForm = $('.modal-form')
  const modalClose = $('.closebtn', modalForm)
  const callBackBtn = $('#phone', 'body')

  modalLabel.on('click', () => {
    modalLabel.addClass('opened')
    modalForm.addClass('shown')
    $('body').addClass('no-scroll')
  })

  callBackBtn.on('click', () => {
    callBackBtn.removeClass('show')
    modalForm.addClass('shown')
    $('body').addClass('no-scroll')
  })

  modalClose.on('click', () => {
    modalForm.removeClass('shown')
    $('body').removeClass('no-scroll')
    modalLabel.removeClass('opened')
  })

  /* Sticky header & modal label */

  const header = document.querySelector('nav')
  let origOffsetY = header.offsetTop

  document.addEventListener('scroll', () => {
    if (window.scrollY >= origOffsetY) {
      header.classList.add('sticky')
      modalLabel.addClass('hidden')
      callBackBtn.addClass('show')
    } else {
      header.classList.remove('sticky')
      modalLabel.removeClass('hidden')
      callBackBtn.removeClass('show')
    }
  })

  /* Portfolio Animations */

  let casesOffset = []
  const caseItem = $('.case', '#portfolio')
  const animationStartDiff = 400

  caseItem.each(function () {
    let item = []
    item.selector = $(this)
    item.offset = $(this).offset().top
    casesOffset.push(item)
  })

  document.addEventListener('scroll', () => {
    for (let i = 0; i < casesOffset.length; i++) {
      if (window.scrollY >= casesOffset[i].offset - animationStartDiff && !(casesOffset[i].selector.hasClass('animate'))) {
        $(casesOffset[i].selector).addClass('animate')
      }
    }

    $(caseItem).each(function () {
      if ($(this).hasClass('animate')) {
        $('.macbook', this).addClass('open')
        $('.tiles', this).addClass('animate')
      }
    })
  })

  /* Tiles to carousel */

  const tiles = $('.tiles', '#portfolio')
  const rebuildButton = $('a.rebuild', '#portfolio')

  rebuildButton.click(function (e) {
    e.preventDefault()
    if (!(tiles.hasClass('rebuild'))) {
      tiles.addClass('rebuild')
      tiles.slick({
        infinite: false,
        slidesToShow: 1,
        dots: true,
        responsive: [
          {
            breakpoint: 769,
            settings: {
              arrows: false
            }
          }
        ]
      })
    }

    $(this).addClass('hidden')
  })

  /* Preferences Animations */

  const prefs = $('.preferences', 'body')
  const prefBtn = $('a.pref-btn', prefs)
  const allAnimation = $('.pref-animation', prefs)
  const allText = $('.pref-text', prefs)

  prefBtn.click(function (e) {
    e.preventDefault()
    prefBtn.removeClass('active')
    $(this).addClass('active')

    let currAnimation = '.' + $(this).attr('id')

    allAnimation.addClass('hidden')
    allText.addClass('hidden')

    $(currAnimation, prefs).removeClass('hidden')

    $(currAnimation, prefs).find('.animation-unit').each(function () {
      $(this).addClass('animate')
    })
  })

  let prefBlockOY = document.querySelector('.preferences').offsetTop

  document.addEventListener('scroll', () => {
    if (window.scrollY >= prefBlockOY - 100) {
      if (prefs.find('a').hasClass('active') === false) {
        $('a.pref-btn#ideas', prefs).addClass('active')
      }
      prefs.find('.ideas').addClass('animate')
    }
  })
})
