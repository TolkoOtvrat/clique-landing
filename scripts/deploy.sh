#!/bin/bash

proot=$(git rev-parse --show-toplevel)

cd $proot

rm -rf web

gulp build

rsync -avz --delete ./web/ clique@snpdev:/home/clique/www/domains/dev/htdocs
